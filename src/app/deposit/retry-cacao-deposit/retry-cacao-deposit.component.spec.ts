import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RetryCacaoDepositComponent } from './retry-cacao-deposit.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('RetryCacaoDepositComponent', () => {
  let component: RetryCacaoDepositComponent;
  let fixture: ComponentFixture<RetryCacaoDepositComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RetryCacaoDepositComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RetryCacaoDepositComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
