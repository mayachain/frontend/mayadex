import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { Router } from '@angular/router';
import { assetAmount, assetToBase } from '@xchainjs/xchain-util';
import { Subscription } from 'rxjs';

import { Asset } from 'src/app/_classes/asset';
import { User } from 'src/app/_classes/user';
import { UserService } from 'src/app/_services/user.service';

@Component({
  selector: 'app-retry-cacao-deposit',
  templateUrl: './retry-cacao-deposit.component.html',
  styleUrls: ['./retry-cacao-deposit.component.scss'],
})
export class RetryCacaoDepositComponent implements OnInit, OnDestroy {
  @Input() asset: Asset;
  @Input() assetAmount: number;
  @Input() cacaoAmount: number;
  @Input() user: User;
  @Input() errorMessage: string;
  @Output() retrySuccess: EventEmitter<string>;
  @Output() withdrawSuccess: EventEmitter<string>;
  @Output() closeModal: EventEmitter<null>;

  cacao: Asset;
  loading: boolean;
  cacaoBalance: number;
  resubmitError: string;
  subs: Subscription[];
  processingMessage: string;
  retryCount: number;

  constructor(private userService: UserService, private router: Router) {
    this.cacao = new Asset('MAYA.CACAO');
    this.retrySuccess = new EventEmitter<string>();
    this.withdrawSuccess = new EventEmitter<string>();
    this.closeModal = new EventEmitter<null>();
    this.processingMessage = '';
    this.retryCount = 0;

    const balances$ = this.userService.userBalances$.subscribe(
      (balances) =>
        (this.cacaoBalance = this.userService.findBalance(balances, this.cacao))
    );

    this.subs = [balances$];
  }

  ngOnInit(): void {
    this.userService.fetchBalances();
  }

  async resubmitCacaoDeposit() {
    this.processingMessage = 'Resubmitting CACAO Deposit';
    this.loading = true;
    this.resubmitError = null;

    // deposit CACAO
    try {
      this.retryCount++;
      const mayaClient = this.user.clients.mayachain;

      // get token address
      const address = this.userService.getTokenAddress(
        this.user,
        this.asset.chain
      );
      if (!address || address === '') {
        console.error('no address found');
        return;
      }

      const cacaoMemo = `+:${this.asset.chain}.${this.asset.symbol}:${address}`;

      const cacaoHash = await mayaClient.deposit({
        amount: assetToBase(assetAmount(this.cacaoAmount)),
        memo: cacaoMemo,
      });

      this.retrySuccess.next(cacaoHash);
    } catch (error) {
      console.error('error retrying CACAO transfer: ', error);
      this.resubmitError = error;
    }

    this.loading = false;
  }

  navigateDepositSymRecovery() {
    this.router.navigate(['/', 'deposit-sym-recovery']);
    this.closeModal.emit();
  }

  ngOnDestroy() {
    for (const sub of this.subs) {
      sub.unsubscribe();
    }
  }
}
