import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ExplorerPathsService {
  binanceExplorerUrl: string;
  bitcoinExplorerUrl: string;
  bchExplorerUrl: string;
  mayachainExplorerUrl: string;
  ethereumExplorerUrl: string;
  litecoinExplorerUrl: string;

  constructor() {
    this.binanceExplorerUrl =
      environment.network === 'testnet'
        ? 'https://testnet-explorer.binance.org'
        : 'https://explorer.binance.org';

    this.bitcoinExplorerUrl =
      environment.network === 'testnet'
        ? 'https://blockstream.info/testnet'
        : 'https://blockstream.info';

    this.mayachainExplorerUrl =
      environment.network === 'testnet'
        ? 'https://explorer.mayachain.info' // flutter web beta
        : 'https://explorer.mayachain.info';

    this.ethereumExplorerUrl =
      environment.network === 'testnet'
        ? 'https://ropsten.etherscan.io'
        : 'https://etherscan.io';

    this.litecoinExplorerUrl =
      environment.network === 'testnet'
        ? 'https://tltc.bitaps.com'
        : 'https://ltc.bitaps.com';

    this.bchExplorerUrl =
      environment.network === 'testnet'
        ? 'https://explorer.bitcoin.com/tbch'
        : 'https://explorer.bitcoin.com/bch';
  }
}
