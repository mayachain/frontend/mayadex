import { TestBed } from '@angular/core/testing';

import { MayachainPricesService } from './mayachain-prices.service';

describe('MayachainPricesService', () => {
  let service: MayachainPricesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MayachainPricesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
