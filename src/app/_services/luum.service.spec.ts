import { TestBed } from '@angular/core/testing';

import { LuumService } from './luum.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('LuumService', () => {
  let service: LuumService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(LuumService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
