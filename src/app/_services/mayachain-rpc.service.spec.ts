import { TestBed } from '@angular/core/testing';

import { MayachainRpcService } from './mayachain-rpc.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('MayachainRpcService', () => {
  let service: MayachainRpcService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(MayachainRpcService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
