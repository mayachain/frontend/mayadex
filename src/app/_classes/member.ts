export interface MemberPool {
  pool: string;
  assetAdded: string;
  assetAddress: string;
  assetWithdrawn: string;
  dateFirstAdded: string;
  dateLastAdded: string;
  liquidityUnits: string;
  cacaoAddress: string;
  cacaoAdded: string;
  cacaoWithdrawn: string;
}

export interface MemberDTO {
  pools: MemberPool[];
}
