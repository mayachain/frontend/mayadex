export interface LiquidityProvider {
  asset: string;
  cacao_address: string;
  asset_address: string;
  last_add_height: number;
  units: string;
  pending_cacao: string;
  pending_asset: string;
  pending_tx_Id: string;
  cacao_deposit_value: string;
  asset_deposit_value: string;
}
