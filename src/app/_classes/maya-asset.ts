export interface MayaAsset {
  asset: string;
  dateCreated: number;
  priceCacao: string;
}
