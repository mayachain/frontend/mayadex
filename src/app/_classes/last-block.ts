export interface LastBlock {
  chain: string;
  lastobservedin: number;
  lastsignedout: number;
  mayachain: number;
}
