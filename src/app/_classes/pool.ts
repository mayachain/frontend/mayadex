export interface PoolDTO {
  asset: string;
  volume24h: string;
  assetDepth: string;
  assetPrice: string;
  assetPriceUSD: string;
  cacaoDepth: string;
  price: string;
  poolAPY: string;
  status: string;
  units: string;
}
