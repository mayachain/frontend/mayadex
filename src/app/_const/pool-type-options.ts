export type PoolTypeOption = 'SYM' | 'ASYM_ASSET' | 'ASYM_CACAO';

export interface AvailablePoolTypeOptions {
  asymAsset: boolean;
  asymCacao: boolean;
  sym: boolean;
}
