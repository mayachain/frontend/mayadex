import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FundsWarningDialogComponent } from './funds-warning-dialog.component';

describe('FundsWarningDialogComponent', () => {
  let component: FundsWarningDialogComponent;
  let fixture: ComponentFixture<FundsWarningDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FundsWarningDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FundsWarningDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
