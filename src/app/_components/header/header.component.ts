import { Component, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { User } from 'src/app/_classes/user';
import { UserService } from 'src/app/_services/user.service';
import { environment } from 'src/environments/environment';
import { FundsWarningDialogComponent } from './funds-warning-dialog/funds-warning-dialog.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnDestroy {
  isTestnet: boolean;
  user: User;
  subs: Subscription[];
  modalDimensions = {
    maxWidth: '520px',
    width: '50vw',
    minWidth: '260px',
  };

  constructor(
    private dialog: MatDialog,
    private userService: UserService
  ) {
    this.isTestnet = environment.network === 'testnet' ? true : false;

    const user$ = this.userService.user$.subscribe(
      (user) => (this.user = user)
    );

    this.subs = [user$];
  }

  openFundsWarningModal() {
    this.dialog.open(FundsWarningDialogComponent, this.modalDimensions);
  }

  ngOnDestroy() {
    for (const sub of this.subs) {
      sub.unsubscribe();
    }
  }
}
