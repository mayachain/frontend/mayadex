import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NativeCacaoPromptComponent } from './native-cacao-prompt.component';

describe('NativeCacaoPromptComponent', () => {
  let component: NativeCacaoPromptComponent;
  let fixture: ComponentFixture<NativeCacaoPromptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NativeCacaoPromptComponent],
      imports: [MatDialogModule, HttpClientTestingModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NativeCacaoPromptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
