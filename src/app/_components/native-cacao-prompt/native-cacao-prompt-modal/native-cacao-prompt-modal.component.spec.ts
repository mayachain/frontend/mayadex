import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Asset } from 'src/app/_classes/asset';

import { NativeCacaoPromptModalComponent } from './native-cacao-prompt-modal.component';

describe('NativeCacaoPromptModalComponent', () => {
  let component: NativeCacaoPromptModalComponent;
  let fixture: ComponentFixture<NativeCacaoPromptModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NativeCacaoPromptModalComponent],
      providers: [
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            asset: new Asset('BNB.CACAO'),
            amount: 1000,
          },
        },
        { provide: MatDialogRef, useValue: { close: () => {} } },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NativeCacaoPromptModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
