import { Component, OnDestroy, OnInit } from '@angular/core';
import { Balance } from '@xchainjs/xchain-client';
import { combineLatest, Subscription } from 'rxjs';
import { User } from '../_classes/user';
import { LuumService } from '../_services/luum.service';
import { UserService } from '../_services/user.service';
import { PoolDTO } from '../_classes/pool';
import { MemberPool } from '../_classes/member';
import { TransactionStatusService } from '../_services/transaction-status.service';
import { isNonNativeCacaoToken } from '../_classes/asset';

@Component({
  selector: 'app-pool',
  templateUrl: './pool.component.html',
  styleUrls: ['./pool.component.scss'],
})
export class PoolComponent implements OnInit, OnDestroy {
  user: User;
  pools: PoolDTO[];
  userPoolError: boolean;
  subs: Subscription[];
  loading: boolean;
  balances: Balance[];
  createablePools: string[];
  memberPools: MemberPool[];
  addresses: string[];
  maxLiquidityCacao: number;
  totalPooledCacao: number;
  depositsDisabled: boolean;
  txStreamInitSuccess: boolean;

  constructor(
    private userService: UserService,
    private luumService: LuumService,
    private txStatusService: TransactionStatusService
  ) {
    this.subs = [];
    this.memberPools = [];
    this.depositsDisabled = false;

    const user$ = this.userService.user$.subscribe((user) => {
      this.user = user;
      this.getAccountPools();
    });

    const balances$ = this.userService.userBalances$.subscribe((balances) => {
      this.balances = balances;
      this.checkCreateableMarkets();
    });

    const pendingTx$ = this.txStatusService.txs$.subscribe((_) => {
      if (!this.txStreamInitSuccess) {
        this.txStreamInitSuccess = true;
      } else {
        setTimeout(() => {
          this.getAccountPools();
        }, 3000);
      }
    });

    this.subs.push(user$, balances$, pendingTx$);
  }

  ngOnInit(): void {
    this.getPools();
    this.getPoolCap();
  }

  getPools() {
    this.luumService.getPools().subscribe((res) => {
      this.pools = res;
      this.checkCreateableMarkets();
    });
  }

  checkCreateableMarkets() {
    if (this.pools && this.balances) {
      this.createablePools = this.balances
        .filter((balance) => {
          const asset = balance.asset;
          return (
            !this.pools.find(
              (pool) => pool.asset === `${asset.chain}.${asset.symbol}`
            ) &&
            !isNonNativeCacaoToken(asset) &&
            asset.chain !== 'MAYA'
          );
        })
        .map((balance) => `${balance.asset.chain}.${balance.asset.symbol}`);
    }
  }

  getPoolCap() {
    const mimir$ = this.luumService.getMimir();
    const network$ = this.luumService.getNetwork();
    const combined = combineLatest([mimir$, network$]);
    const sub = combined.subscribe(([mimir, network]) => {
      // prettier-ignore
      this.totalPooledCacao = +network.totalPooledCacao / (10 ** 8);

      if (mimir && mimir['mimir//MAXIMUMLIQUIDITYCACAO']) {
        // prettier-ignore
        this.maxLiquidityCacao = mimir['mimir//MAXIMUMLIQUIDITYCACAO'] / (10 ** 8);
        this.depositsDisabled =
          this.totalPooledCacao / this.maxLiquidityCacao >= 0.9;
      }
    });

    this.subs.push(sub);
  }

  async getAddresses(): Promise<string[]> {
    if (this.user && this.user.type === 'metamask') {
      return [this.user.wallet.toLowerCase()];
    } else {
      const mayaClient = this.user.clients.mayachain;
      const mayaAddress = mayaClient.getAddress();
      console.log(mayaAddress)

      const btcClient = this.user.clients.bitcoin;
      const btcAddress = btcClient.getAddress();

      const ltcClient = this.user.clients.litecoin;
      const ltcAddress = ltcClient.getAddress();

      const bchClient = this.user.clients.bitcoinCash;
      const bchAddress = bchClient.getAddress();

      const bnbClient = this.user.clients.binance;
      const bnbAddress = bnbClient.getAddress();

      const ethClient = this.user.clients.ethereum;
      const ethAddress = ethClient.getAddress();

      return [
        mayaAddress,
        btcAddress,
        ltcAddress,
        bchAddress,
        bnbAddress,
        ethAddress,
      ];
    }
  }

  async getAccountPools() {
    this.loading = true;
    this.memberPools = [];

    if (this.user) {
      if (!this.addresses) {
        this.addresses = await this.getAddresses();
      }

      for (const address of this.addresses) {
        this.luumService
          .getMember(address.toLowerCase())
          .subscribe((res) => {
            for (const pool of res.pools) {
              const match = this.memberPools.find(
                (existingPool) => existingPool.pool === pool.pool
              );
              if (!match) {
                const memberPools = this.memberPools;
                memberPools.push(pool);
                this.memberPools = [...memberPools];
              }
            }
          });
      }
    }

    this.loading = false;
  }

  ngOnDestroy(): void {
    for (const sub of this.subs) {
      sub.unsubscribe();
    }
  }
}
