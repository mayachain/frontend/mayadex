const { writeFile } = require('fs');
const { argv } = require('yargs');
// read environment variables from .env file
require('dotenv').config();
// read the command line arguments passed with yargs
const environment = argv.environment;
const network = argv.network;

const isProduction = environment === 'prod';

let targetPath: string;

if (isProduction) {
  switch (network) {
    case 'mainnet':
      targetPath = `./src/environments/environment.prod.ts`; // mainnet prod
      break;
    case 'testnet':
      targetPath = `./src/environments/environment.testnet.ts`; // testnet prod
      break;
    case 'stagenet':
      targetPath = `./src/environments/environment.stagenet.ts`; // testnet prod
  }
} else {
  targetPath = `./src/environments/environment.ts`; // testnet dev
}

// we have access to our environment variables
// in the process.env object thanks to dotenv
const environmentFileContent = `
// prettier-ignore
import { Network } from '@xchainjs/xchain-client';

export const environment: {
  production: boolean;
  network: Network;
  etherscanKey: string;
  infuraProjectId: string;
  appLocked: boolean;
} = {
   production: ${isProduction},
   network: Network.${network.charAt(0).toUpperCase() + network.slice(1)},
   etherscanKey: '${process.env.ETHERSCAN_KEY}',
   infuraProjectId: '${process.env.INFURA_PROJECT_ID}',
   appLocked: ${process.env.APP_LOCKED ?? false}
};
`;
// write the content to the respective file
writeFile(targetPath, environmentFileContent, (err) => {
  if (err) {
    console.log(err);
  }
  console.log(`Wrote variables to ${targetPath}`);
});
