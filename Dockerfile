# build environment
FROM node:14.17.3-alpine as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

RUN apk update && apk add git
COPY package.json /app/package.json
COPY package-lock.json /app/package-lock.json
RUN npm config set unsafe-perm true
RUN npm install
RUN npm install @angular/cli@11.2.4 -g
COPY . /app
RUN npm run build:testnet

# docker environment
FROM nginx:1.16.0-alpine
COPY --from=build /app/dist/mayadex /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY etc/nginx/nginx.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

