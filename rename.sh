#!/bin/sh

default_package_name=${1:-@xchainjs}
package_name=${2:-@xchainjs}

comby "${default_package_name}" "${package_name}" \
  -matcher .md -directory src -exclude-dir node_modules -in-place \
  -jobs 12 -depth 5

